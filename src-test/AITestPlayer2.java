

import static org.junit.Assert.*;

import org.junit.*;

import com.atomicobject.othello.AI;
import com.atomicobject.othello.CannedMoveGenerator;
import com.atomicobject.othello.FirstMoveFoundGenerator;
import com.atomicobject.othello.GameState;
import com.atomicobject.othello.MoveGenerator;


public class AITestPlayer2 {

	@Test
	public void testFindFirstMoveRightPlayerOne() {
		// for your real implementation.
				MoveGenerator firstMoveGen = new FirstMoveFoundGenerator();
				AI ai = new AI(firstMoveGen);
				GameState state = new GameState();
				state.setPlayer(2);
				state.setBoard(new int[][]{{0, 0, 0, 0, 0, 0, 0, 0},
						                   {0, 0, 0, 0, 0, 0, 0, 0},
						                   {0, 0, 0, 0, 0, 0, 0, 0},
						                   {0, 0, 0, 1, 2, 0, 0, 0},
						                   {0, 0, 0, 2, 1, 0, 0, 0},
						                   {0, 0, 0, 0, 0, 0, 0, 0},
						                   {0, 0, 0, 0, 0, 0, 0, 0},
						                   {0, 0, 0, 0, 0, 0, 0, 0}});
				int[] foundMove = ai.computeMove(state);
				assertArrayEquals(new int[] {3, 2} , foundMove);
	}
}
