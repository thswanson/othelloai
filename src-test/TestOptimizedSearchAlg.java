import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Test;

import com.atomicobject.othello.AI;
import com.atomicobject.othello.FirstMoveFoundGenerator;
import com.atomicobject.othello.GameState;
import com.atomicobject.othello.MoveGenerator;
import com.atomicobject.othello.OptimizedMoveGenerator;
import com.atomicobject.othello.AllMoveSearchAlg;
import com.atomicobject.othello.SearchAlgorithms;

public class TestOptimizedSearchAlg {

	@Test
	public void testAlgorithmFindsAllMoves() {
			
				MoveGenerator firstMoveGen = new OptimizedMoveGenerator();
				AI ai = new AI(firstMoveGen);
				GameState state = new GameState();
				state.setPlayer(1);
				state.setBoard(new int[][]{{0, 0, 0, 0, 0, 0, 0, 0},
						                   {0, 0, 0, 0, 0, 0, 0, 0},
						                   {0, 0, 0, 0, 0, 0, 0, 0},
						                   {0, 0, 0, 1, 2, 0, 0, 0},
						                   {0, 0, 0, 2, 1, 0, 0, 0},
						                   {0, 0, 0, 0, 0, 0, 0, 0},
						                   {0, 0, 0, 0, 0, 0, 0, 0},
						                   {0, 0, 0, 0, 0, 0, 0, 0}});
				ai.computeMove(state);
				ArrayList<int[]> test = ((OptimizedMoveGenerator) firstMoveGen).getAllMovesGeneratedAtOnePosition();
				assertFalse(test.isEmpty());
				ArrayList<int []> expected = new ArrayList<int []>();
				expected.add(new int[] {3, 5});
				expected.add(new int[] {2, 4});
				expected.add(new int[] {4, 2});
				expected.add(new int[] {5, 3});
				//int i = 0;
				//assertTrue(test.get(0).equals(new int[] {3, 5}));
				for (int i = 0 ; i < 4 ; i ++) {
					assertArrayEquals(test.get(i), expected.get(i));
					//assertTrue();
				}
				//assertTrue(test.equals(expected));
	}
	@Test
	public void testMoreComplicatedAllMoves() {
		MoveGenerator firstMoveGen = new OptimizedMoveGenerator();
		AI ai = new AI(firstMoveGen);
		GameState state = new GameState();
		state.setPlayer(1);
		state.setBoard(new int[][]{{0, 0, 0, 0, 0, 0, 0, 0},
				                   {0, 0, 0, 0, 2, 0, 0, 0},
				                   {0, 0, 0, 0, 2, 2, 0, 0},
				                   {0, 0, 0, 1, 2, 2, 0, 0},
				                   {0, 0, 0, 2, 1, 0, 0, 0},
				                   {0, 0, 0, 0, 0, 0, 0, 0},
				                   {0, 0, 0, 0, 0, 0, 0, 0},
				                   {0, 0, 0, 0, 0, 0, 0, 0}});
		ai.computeMove(state);
		ArrayList<int[]> test = ((OptimizedMoveGenerator) firstMoveGen).getAllMovesGeneratedAtOnePosition();
		assertFalse(test.isEmpty());
		ArrayList<int []> expected = new ArrayList<int []>();
		
		expected.add(new int[] {0, 4});
		expected.add(new int[] {1, 5});
		expected.add(new int[] {3, 6});
		
		expected.add(new int[] {4, 2});
		expected.add(new int[] {5, 3});
		
		//int i = 0;
		//assertTrue(test.get(0).equals(new int[] {3, 5}));
		for (int i = 0 ; i < 5 ; i ++) {
			System.out.println("test"+test.get(i)[0]+""+test.get(i)[1] + "\n" + 
					"expect"+expected.get(i)[0]+ ""+ expected.get(i)[1]);
			assertArrayEquals(test.get(i), expected.get(i));
			//assertTrue();
		}
	}
	
}
