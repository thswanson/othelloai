
import static org.junit.Assert.*;

import org.junit.*;

import com.atomicobject.othello.AI;
import com.atomicobject.othello.CannedMoveGenerator;
import com.atomicobject.othello.FirstMoveFoundGenerator;
import com.atomicobject.othello.FirstMoveSearchAlg;
import com.atomicobject.othello.GameState;
import com.atomicobject.othello.MoveGenerator;
import com.atomicobject.othello.SearchAlgorithms;


public class AITest {

	@Test
	public void test() {
		// This is just an example test to show JUnit working. It won't be useful
		// for your real implementation.
		MoveGenerator cannedGen = new CannedMoveGenerator(new int[][] {{2, 4}, {3, 5}});
		AI ai = new AI(cannedGen);
		GameState state = new GameState();
		state.setPlayer(1);
		state.setBoard(new int[][]{{0, 0, 0, 0, 0, 0, 0, 0},
				                   {0, 0, 0, 0, 0, 0, 0, 0},
				                   {0, 0, 0, 0, 0, 0, 0, 0},
				                   {0, 0, 0, 1, 2, 0, 0, 0},
				                   {0, 0, 0, 2, 1, 0, 0, 0},
				                   {0, 0, 0, 0, 0, 0, 0, 0},
				                   {0, 0, 0, 0, 0, 0, 0, 0},
				                   {0, 0, 0, 0, 0, 0, 0, 0}});
		
		// Our first canned move is [2,4]
		assertArrayEquals(new int[]{2, 4}, ai.computeMove(state));

		// Our second canned move is [3, 5]
		assertArrayEquals(new int[]{3, 5}, ai.computeMove(state));
	}
	
	@Test
	public void testCannedMoveGenerator() {
		// This is just an example test to show JUnit working. It won't be useful
		// for your real implementation.
		MoveGenerator cannedGen = new CannedMoveGenerator(new int[][] {{2, 4}, {3, 5}});
		AI ai = new AI(cannedGen);
		GameState state = new GameState();
		state.setPlayer(1);
		state.setBoard(new int[][]{{0, 0, 0, 0, 0, 0, 0, 0},
				                   {0, 0, 0, 0, 0, 0, 0, 0},
				                   {0, 0, 0, 0, 0, 0, 0, 0},
				                   {0, 0, 0, 1, 2, 0, 0, 0},
				                   {0, 0, 0, 2, 1, 0, 0, 0},
				                   {0, 0, 0, 0, 0, 0, 0, 0},
				                   {0, 0, 0, 0, 0, 0, 0, 0},
				                   {0, 0, 0, 0, 0, 0, 0, 0}});
		// Our first canned move is [2,4]
		assertArrayEquals(new int[]{2, 4}, ai.computeMove(state));

				// Our second canned move is [3, 5]
		assertArrayEquals(new int[]{3, 5}, ai.computeMove(state));
	}
	
	@Test
	public void testFindFirstMoveRightPlayerOne() {
		// for your real implementation.
				//SearchAlgorithms FirstMoveSearchAlg = new FirstMoveSearchAlg();
				MoveGenerator firstMoveGen = new FirstMoveFoundGenerator();
				AI ai = new AI(firstMoveGen);
				GameState state = new GameState();
				state.setPlayer(1);
				state.setBoard(new int[][]{{0, 0, 0, 0, 0, 0, 0, 0},
						                   {0, 0, 0, 0, 0, 0, 0, 0},
						                   {0, 0, 0, 0, 0, 0, 0, 0},
						                   {0, 0, 0, 1, 2, 0, 0, 0},
						                   {0, 0, 0, 2, 1, 0, 0, 0},
						                   {0, 0, 0, 0, 0, 0, 0, 0},
						                   {0, 0, 0, 0, 0, 0, 0, 0},
						                   {0, 0, 0, 0, 0, 0, 0, 0}});
				int[] foundMove = ai.computeMove(state);
				assertArrayEquals(new int[] {3, 5} , foundMove);
	}
	
	@Test
	public void testFindFirstMoveLeftPlayerOne() {
		// for your real implementation.
				MoveGenerator firstMoveGen = new FirstMoveFoundGenerator();
				AI ai = new AI(firstMoveGen);
				GameState state = new GameState();
				state.setPlayer(1);
				state.setBoard(new int[][]{{0, 0, 0, 0, 0, 0, 0, 0},
						                   {0, 0, 0, 0, 0, 0, 0, 0},
						                   {0, 0, 0, 0, 0, 0, 0, 0},
						                   {0, 0, 0, 2, 1, 0, 0, 0},
						                   {0, 0, 0, 1, 2, 0, 0, 0},
						                   {0, 0, 0, 0, 0, 0, 0, 0},
						                   {0, 0, 0, 0, 0, 0, 0, 0},
						                   {0, 0, 0, 0, 0, 0, 0, 0}});
				int[] foundMove = ai.computeMove(state);
				assertArrayEquals(new int[] {3, 2} , foundMove);
	}
	
	@Test
	public void testFindRightLongMovePlayerOne() {
		// for your real implementation.
				MoveGenerator firstMoveGen = new FirstMoveFoundGenerator();
				AI ai = new AI(firstMoveGen);
				GameState state = new GameState();
				state.setPlayer(1);
				state.setBoard(new int[][]{{0, 0, 0, 0, 0, 0, 0, 0},
						                   {0, 0, 0, 0, 0, 0, 0, 0},
						                   {0, 0, 0, 0, 0, 0, 0, 0},
						                   {0, 0, 0, 1, 2, 2, 2, 0},
						                   {0, 0, 0, 2, 1, 0, 0, 0},
						                   {0, 0, 0, 0, 0, 0, 0, 0},
						                   {0, 0, 0, 0, 0, 0, 0, 0},
						                   {0, 0, 0, 0, 0, 0, 0, 0}});
				int[] foundMove = ai.computeMove(state);
				assertArrayEquals(new int[] {3, 7} , foundMove);
	}
	
	@Test
	public void testFindLongMovePlayerOne() {
		// for your real implementation.
				MoveGenerator firstMoveGen = new FirstMoveFoundGenerator();
				AI ai = new AI(firstMoveGen);
				GameState state = new GameState();
				state.setPlayer(1);
				state.setBoard(new int[][]{{0, 0, 0, 0, 0, 0, 0, 0},
						                   {0, 0, 0, 0, 0, 0, 0, 0},
						                   {0, 0, 0, 0, 0, 0, 0, 0},
						                   {0, 2, 2, 2, 1, 0, 0, 0},
						                   {0, 0, 0, 1, 2, 0, 0, 0},
						                   {0, 0, 0, 0, 0, 0, 0, 0},
						                   {0, 0, 0, 0, 0, 0, 0, 0},
						                   {0, 0, 0, 0, 0, 0, 0, 0}});
				int[] foundMove = ai.computeMove(state);
				assertArrayEquals(new int[] {3, 0} , foundMove);
	}
	
	@Test
	public void testNoFirstRowMovePlayerOne() {
		// for your real implementation.
				MoveGenerator firstMoveGen = new FirstMoveFoundGenerator();
				AI ai = new AI(firstMoveGen);
				GameState state = new GameState();
				state.setPlayer(1);
				state.setBoard(new int[][]{{0, 0, 0, 2, 2, 0, 0, 0},
						                   {0, 0, 0, 2, 2, 0, 0, 0},
						                   {0, 0, 0, 2, 2, 0, 0, 0},
						                   {2, 2, 2, 2, 2, 2, 0, 2},
						                   {0, 0, 0, 2, 2, 0, 0, 1},
						                   {0, 0, 0, 2, 2, 0, 0, 0},
						                   {0, 0, 0, 2, 2, 0, 0, 0},
						                   {0, 0, 0, 2, 2, 0, 0, 0}});
				int[] foundMove = ai.computeMove(state);
				assertArrayEquals(new int[] {2, 7} , foundMove);
	}
	
	@Test
	public void testMoveAbove() {
		// for your real implementation.
				MoveGenerator firstMoveGen = new FirstMoveFoundGenerator();
				AI ai = new AI(firstMoveGen);
				GameState state = new GameState();
				state.setPlayer(1);
				state.setBoard(new int[][]{{0, 0, 0, 0, 0, 0, 0, 0},
						                   {0, 0, 0, 0, 0, 0, 0, 0},
						                   {0, 0, 0, 0, 0, 0, 0, 0},
						                   {0, 0, 0, 1, 2, 2, 2, 2},
						                   {0, 0, 0, 2, 1, 0, 0, 0},
						                   {0, 0, 0, 0, 0, 0, 0, 0},
						                   {0, 0, 0, 0, 0, 0, 0, 0},
						                   {0, 0, 0, 0, 0, 0, 0, 0}});
				int[] foundMove = ai.computeMove(state);
				assertArrayEquals(new int[] {2, 4} , foundMove);
	}
	
	@Test
	public void testMoveBelow() {
		// for your real implementation.
				MoveGenerator firstMoveGen = new FirstMoveFoundGenerator();
				AI ai = new AI(firstMoveGen);
				GameState state = new GameState();
				state.setPlayer(1);
				state.setBoard(new int[][]{{0, 0, 0, 0, 0, 0, 0, 0},
						                   {0, 0, 0, 0, 0, 0, 0, 0},
						                   {0, 0, 0, 0, 0, 0, 0, 0},
						                   {0, 0, 0, 1, 1, 0, 0, 0},
						                   {0, 0, 0, 1, 2, 2, 2, 2},
						                   {0, 0, 0, 0, 0, 0, 0, 0},
						                   {0, 0, 0, 0, 0, 0, 0, 0},
						                   {0, 0, 0, 0, 0, 0, 0, 0}});
				int[] foundMove = ai.computeMove(state);
				assertArrayEquals(new int[] {5, 4} , foundMove);
	}
	
	@Test
	public void testMoveNotAllowed() {
		// for your real implementation.
				MoveGenerator firstMoveGen = new FirstMoveFoundGenerator();
				AI ai = new AI(firstMoveGen);
				GameState state = new GameState();
				state.setPlayer(1);
				state.setBoard(new int[][]{{0, 0, 0, 0, 0, 0, 0, 0},
						                   {0, 0, 0, 0, 0, 0, 0, 0},
						                   {0, 0, 0, 0, 0, 0, 0, 0},
						                   {0, 0, 0, 0, 0, 0, 0, 0},
						                   {0, 0, 0, 0, 0, 0, 0, 0},
						                   {0, 0, 0, 0, 0, 0, 0, 0},
						                   {0, 0, 0, 0, 0, 0, 0, 0},
						                   {0, 0, 0, 0, 0, 0, 0, 0}});
				int[] foundMove = ai.computeMove(state);
				assertFalse(new int[] {5, 4}.equals(foundMove));
	}
	

	@Test
	public void testMoveNotAllowedTwo() {
		// for your real implementation.
				MoveGenerator firstMoveGen = new FirstMoveFoundGenerator();
				AI ai = new AI(firstMoveGen);
				GameState state = new GameState();
				state.setPlayer(1);
				state.setBoard(new int[][]{{0, 0, 0, 0, 2, 2, 0, 0},
						                   {0, 0, 1, 0, 2, 0, 0, 0},
						                   {0, 0, 1, 2, 0, 0, 0, 0},
						                   {0, 0, 0, 0, 0, 0, 0, 0},
						                   {0, 0, 0, 0, 0, 0, 0, 0},
						                   {0, 0, 0, 0, 0, 0, 0, 0},
						                   {0, 0, 0, 0, 0, 0, 0, 0},
						                   {0, 0, 0, 0, 0, 0, 0, 0}});
				int[] foundMove = ai.computeMove(state);
				//System.out.println( "\n" + "\n" + "\n" +foundMove[0] + ""+ foundMove[1]);
				assertArrayEquals(new int[] {2, 4} , foundMove);
	}
	@Test
	public void testEasyDiag() {
		// for your real implementation.
				MoveGenerator firstMoveGen = new FirstMoveFoundGenerator();
				AI ai = new AI(firstMoveGen);
				GameState state = new GameState();
				state.setPlayer(1);
				state.setBoard(new int[][]{{0, 0, 0, 0, 0, 0, 0, 0},
						                   {0, 0, 0, 0, 0, 0, 0, 0},
						                   {0, 0, 0, 0, 0, 0, 0, 0},
						                   {0, 0, 0, 1, 0, 0, 0, 0},
						                   {0, 0, 0, 0, 2, 0, 0, 0},
						                   {0, 0, 0, 0, 0, 0, 0, 0},
						                   {0, 0, 0, 0, 0, 0, 0, 0},
						                   {0, 0, 0, 0, 0, 0, 0, 0}});
				int[] foundMove = ai.computeMove(state);
				//System.out.println( "\n" + "\n" + "\n" +foundMove[0] + ""+ foundMove[1]);
				assertArrayEquals(new int[] {5, 5} , foundMove);
	}
	
	@Test
	public void testHarderDiag() {
		// for your real implementation.
				MoveGenerator firstMoveGen = new FirstMoveFoundGenerator();
				AI ai = new AI(firstMoveGen);
				GameState state = new GameState();
				state.setPlayer(1);
				state.setBoard(new int[][]{{0, 0, 0, 0, 0, 0, 0, 0},
						                   {0, 0, 0, 0, 0, 0, 0, 0},
						                   {0, 0, 0, 1, 0, 0, 0, 0},
						                   {0, 0, 0, 0, 2, 0, 0, 0},
						                   {0, 0, 0, 0, 2, 2, 0, 0},
						                   {0, 0, 0, 0, 0, 2, 2, 0},
						                   {0, 0, 0, 0, 0, 0, 0, 0},
						                   {0, 0, 0, 0, 0, 0, 0, 0}});
				int[] foundMove = ai.computeMove(state);
				//System.out.println( "\n" + "\n" + "\n" +foundMove[0] + ""+ foundMove[1]);
				assertArrayEquals(new int[] {6, 7} , foundMove);
	}
	
	@Test
	public void testHarderBackwardsDiag() {
		// for your real implementation.
				MoveGenerator firstMoveGen = new FirstMoveFoundGenerator();
				AI ai = new AI(firstMoveGen);
				GameState state = new GameState();
				state.setPlayer(1);
				state.setBoard(new int[][]{{0, 0, 0, 0, 0, 0, 0, 0},
						                   {0, 0, 0, 0, 0, 0, 0, 0},
						                   {0, 0, 0, 2, 0, 0, 0, 0},
						                   {0, 0, 0, 0, 2, 0, 0, 0},
						                   {0, 0, 0, 0, 2, 2, 0, 0},
						                   {0, 0, 0, 0, 0, 0, 1, 0},
						                   {0, 0, 0, 0, 0, 0, 0, 0},
						                   {0, 0, 0, 0, 0, 0, 0, 0}});
				int[] foundMove = ai.computeMove(state);
				//System.out.println( "\n" + "\n" + "\n" +foundMove[0] + ""+ foundMove[1]);
				assertArrayEquals(new int[] {1, 2} , foundMove);
	}
	
	@Test
	public void testDiagReflected() {
		// for your real implementation.
				MoveGenerator firstMoveGen = new FirstMoveFoundGenerator();
				AI ai = new AI(firstMoveGen);
				GameState state = new GameState();
				state.setPlayer(1);
				state.setBoard(new int[][]{{0, 0, 0, 0, 0, 0, 0, 0},
						                   {0, 0, 0, 0, 0, 0, 0, 0},
						                   {0, 0, 0, 2, 0, 0, 0, 0},
						                   {0, 0, 0, 0, 2, 0, 0, 0},
						                   {0, 0, 0, 2, 0, 2, 0, 0},
						                   {0, 0, 2, 0, 0, 0, 0, 0},
						                   {0, 1, 0, 0, 0, 0, 0, 0},
						                   {0, 0, 0, 0, 0, 0, 0, 0}});
				int[] foundMove = ai.computeMove(state);
				//System.out.println( "\n" + "\n" + "\n" +foundMove[0] + ""+ foundMove[1]);
				assertArrayEquals(new int[] {2, 5} , foundMove);
	}
	
	@Test
	public void testDiagReflectedOtherSide() {
		// for your real implementation.
				MoveGenerator firstMoveGen = new FirstMoveFoundGenerator();
				AI ai = new AI(firstMoveGen);
				GameState state = new GameState();
				state.setPlayer(1);
				state.setBoard(new int[][]{{0, 0, 0, 0, 0, 0, 0, 0},
						                   {0, 0, 0, 0, 0, 0, 0, 0},
						                   {0, 0, 0, 2, 0, 1, 0, 0},
						                   {0, 0, 0, 0, 2, 0, 0, 0},
						                   {0, 0, 0, 2, 0, 2, 0, 0},
						                   {0, 0, 2, 0, 0, 0, 0, 0},
						                   {0, 2, 0, 0, 0, 0, 0, 0},
						                   {0, 0, 0, 0, 0, 0, 0, 0}});
				int[] foundMove = ai.computeMove(state);
				//System.out.println( "\n" + "\n" + "\n" +foundMove[0] + ""+ foundMove[1]);
				assertArrayEquals(new int[] {7, 0} , foundMove);
	}
	
	
}
