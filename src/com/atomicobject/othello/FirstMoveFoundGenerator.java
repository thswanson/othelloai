package com.atomicobject.othello;

public class FirstMoveFoundGenerator implements MoveGenerator {

	GameState state;
	int [][] board;
	int [] moveToReturn;
	SearchAlgorithms searchAlgs;
	
	@Override
	public int[] returnMove() {
		moveToReturn = findPossibleMoves();
		return moveToReturn;
	}

	@Override
	public void setGameState(GameState state) {
		moveToReturn = null;
		this.state = state;
		board = this.state.getBoard();
		searchAlgs = new FirstMoveSearchAlg (state, state.getPlayer());
	}
	
	//Find a move to place
	public int [] findPossibleMoves() {
		
		for (int i = 0; i < board.length ; i ++) {
			for (int j = 0; j < board.length ; j++ ) {
				int currentPiece = board[i][j];
				switch(currentPiece) {
					case(0): 
						//do something
						break;
					case(1) :
						if (1 != state.getPlayer()) {
							if (searchAlgs.checkForMove(i,j)) return searchAlgs.getMoveToReturn();
						}
						break;
					case(2) : 
						if (2 != state.getPlayer()) {
							if (searchAlgs.checkForMove(i,j)) return searchAlgs.getMoveToReturn();
						}
						break;
				}
			}
		}
		return null;
		
	}
}
	
