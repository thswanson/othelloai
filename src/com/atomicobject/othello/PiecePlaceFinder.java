package com.atomicobject.othello;


/*
* This attempts to find a place to put a piece after a potential move has been located! It should be capable of outputting a variable
*amount of moves found. This is good for being used for a strategy that returns the first move found or a strategy that may want
*to calculate the best move before playing! */

import java.util.ArrayList;

public class PiecePlaceFinder {
	private int [][] board;
	//int[] moveToReturn;
	private int player;
	private ArrayList<int []> allPossibleMoves;
	
	
	public PiecePlaceFinder(int[][] board, int player) {
		this.board = board;
		this.player = player;
		allPossibleMoves = new ArrayList<int []> () ;
		// TODO Auto-generated constructor stub
	}
	
	public ArrayList<int []>  getMovesToReturn() {
		return allPossibleMoves;
	}
	
	boolean checkRightToLeft(int row, int col, int i) {
		if (i < col) {
			//if we found an "anvil" before we found the flippable piece
			col++;
			for( ; col <board.length; col++) {
				if (board[row][col] == player) return false;
				else if(board[row][col] == 0 ) {
					 allPossibleMoves.add(new int [] {row, col});
					 return true;
				}
			}
		}
		else {
			for( ;col >= 0; col--) {
				if (board[row][col] == player) return false;
				else if(board[row][col] == 0 ) {
					 allPossibleMoves.add(new int [] {row, col});
					return true; }
			}
		}
		return false;
	}
    boolean checkBottomToTop(int row, int col, int i) {
		if (i < row) {
			row++;
			for( ; row <board.length; row++) {
				if (board[row][col] == player) return false;
				else if(board[row][col] == 0 ) {
					 allPossibleMoves.add(new int [] {row, col});
					 return true;
				}
			}
		}
		else {
			for( ;row >= 0; row--) {
				if (board[row][col] == player) return false;
				else if(board[row][col] == 0 ) {
					 allPossibleMoves.add(new int [] {row, col});
					return true; }
			}
		}
		return false;
	}
	
	boolean checkDiagBotLToTopR(int row, int col, int rowPorMi) {
		if ( rowPorMi <  row ) {
			for (int i = 1 ; (row + i < board.length && col - i >= 0) ; i ++ ) {
				if (board[row+i][col-i] == player) return false;
				else if(board[row+i][col-i] == 0 ) {
					 allPossibleMoves.add(new int [] {row+i, col-i});
					return true;
				}
			}
		}
		else {
			for (int i = 1 ; (row - i >= 0 && col + i < board.length) ; i ++ ) {
				if (board[row-i][col+i] == player) return false;
				else if(board[row-i][col+i] == 0 ) {
					 allPossibleMoves.add(new int [] {row-i, col+i});
					return true;
				}
			}
		}
		return false;
	}
	boolean checkDiagTopLToBotR(int row, int col, int rowPorMi) {
		if ( rowPorMi <  row ) {
			for (int i = 1 ; i < board.length - Integer.max(row,col) ; i ++ ) {
				if (board[row+i][col+i] == player) return false;
				else if(board[row+i][col+i] == 0 ) {
					allPossibleMoves.add(new int [] {row+i, col+i});
					return true;
				}
			}
		}
		else {
			for (int i = 1 ; i <= Integer.min(row,col) ; i ++ ) {
				if (board[row-i][col-i] == player) return false;
				else if(board[row-i][col-i] == 0 ) {
					allPossibleMoves.add(new int [] {row-i, col-i});
					return true;
				}
			}
		}
		return false;
	}

}
