package com.atomicobject.othello;

import java.util.Arrays;
import java.util.ListIterator;

public class CannedMoveGenerator implements MoveGenerator {


	ListIterator<int[]> moveList;
	
	public CannedMoveGenerator (int[][] moves) {
		moveList = Arrays.asList(moves).listIterator();
	}
	
	
	@Override
	public int[] returnMove() {
		// TODO Auto-generated method stub
		return moveList.next();
	}


	@Override
	public void setGameState(GameState state) {
		
	}


}
