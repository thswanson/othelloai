package com.atomicobject.othello;


/* This is an implementation of the SearchAlgorithm interface. This strategy will return the first move found no matter what
 * 
 */

public class FirstMoveSearchAlg implements SearchAlgorithms {
	private GameState state;
	private int[][] board;
	private int [] moveToReturn;
	private int player;
	private int opposingPlayer;
	private PiecePlaceFinder placeFinder;
	
	
	public FirstMoveSearchAlg(GameState state, int player) {
		this.state = state;
		board = this.state.getBoard();
		placeFinder = new PiecePlaceFinder(board, player);
		//MoveScourer scourer = new MoveScourer(placeFinder);
		setPlayer(player);
	}
	
	public void setPlayer(int player) {
		if (player == 1 ) {
			this.player = player;
			opposingPlayer = 2;
		}
		else {
			this.player = player;
			opposingPlayer = 1;
		}
	}
	@Override
	public int[] getMoveToReturn() {
		return moveToReturn;
	}

	//Exaughstively search the board for potential moves related to a passed in location, immediately returning true when a valid one is found
	public boolean checkForMove(int row , int col) {
		if(checkLeft(row,col)) { return true;}
		else if(checkRight(row,col)) return true;
		else if(checkUp(row,col)) return true;
		else if(checkDown(row,col)) return true;
		else if(checkUpLeft(row,col)) return true;
		else if(checkBotRight(row,col)) return true;
		else if(checkUpRight(row,col)) return true;
		else if(checkBotLeft(row,col)) return true;
		else { return false; }
	}

	private boolean checkBotLeft(int row, int col) {
		for(int i = 1 ; (row + i < board.length && col - i >= 0)  ; i ++ ) {
			if (board[row+i][col-i]== opposingPlayer) continue;
			else if (board[row+i][col-i]== player)  { if(placeFinder.checkDiagBotLToTopR(row , col , row+i)) {
				moveToReturn = placeFinder.getMovesToReturn().get(0); 
				return true; }
			}
			else  { break; }
		}
		return false;
	}

	private boolean checkUpRight(int row, int col) {
		for(int i = 1 ; (row - i >= 0 && col + i < board.length)  ; i ++ ) {
			if (board[row-i][col+i]== opposingPlayer) continue;
			else if (board[row-i][col+i]== player)  { 
				if(placeFinder.checkDiagBotLToTopR(row , col , row-i)) {
					moveToReturn = placeFinder.getMovesToReturn().get(0); 
					return true; }
			}
			else  { break; }
		}
		return false;
	}

	private boolean checkBotRight(int row, int col) {
		int max = Integer.max(row, col);
		for(int i = 1 ; i < board.length - max ; i ++ ) {
			if (board[row+i][col+i]== opposingPlayer) continue;
			else if (board[row+i][col+i]== player)  { 
				if(placeFinder.checkDiagTopLToBotR(row , col , row + i)) {
					moveToReturn = placeFinder.getMovesToReturn().get(0); 
					return true; }
			}
			else  { break; }
		}
		return false;
	}

	private boolean checkUpLeft(int row, int col) {
		int min = Integer.min(row, col);
		for(int i = 1 ; i <= min ; i ++ ) {
			if (board[row-i][col-i]== opposingPlayer) continue;
			else if (board[row-i][col-i]== player)  { 
				if(placeFinder.checkDiagTopLToBotR(row , col , row-i)) {
					moveToReturn = placeFinder.getMovesToReturn().get(0); 
					return true; }
			}
			else  { break; }
		}
		return false;
	}

	private boolean checkDown(int row, int col) {
		for(int i = row+1 ; i < board.length ; i ++ ) {
			if (board[i][col]== opposingPlayer) continue;
			else if (board[i][col]== player)  { 
				if(placeFinder.checkBottomToTop(row , col , i)) {
					moveToReturn = placeFinder.getMovesToReturn().get(0); 
					return true; }
			}
			else  { break; }
		}
		return false;
	}

	private boolean checkUp(int row, int col) {
		for ( int i = row-1; i >=0 ; i--) {
			if (board[i][col]== opposingPlayer) continue;
			else if (board[i][col]== player)  { 
				if(placeFinder.checkBottomToTop(row , col , i)) {
					moveToReturn = placeFinder.getMovesToReturn().get(0); 
					return true; }
			}
			else  { break; }
		}
		return false;
	}

	private boolean checkLeft(int row, int col) {
		for ( int i = col-1; i >=0 ; i--) {
			if (board[row][i] == opposingPlayer) continue;
			else if (board[row][i]== player)  { 
				if(placeFinder.checkRightToLeft(row , col , i)) {
					moveToReturn = placeFinder.getMovesToReturn().get(0); 
					return true; } 
			}
			else  { break; }
		}
		return false;
	}
	
	private boolean checkRight(int row, int col) {
		for(int i = col+1 ; i < board.length ; i ++ ) {
			if (board[row][i ]== opposingPlayer) continue;
			else if (board[row][i]== player)  { 
				if(placeFinder.checkRightToLeft(row , col , i)) {
					moveToReturn = placeFinder.getMovesToReturn().get(0); 
					return true; }
			}
			else  { break; }
		}
		return false;
	}

}
