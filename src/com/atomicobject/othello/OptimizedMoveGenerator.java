package com.atomicobject.othello;

import java.util.ArrayList;

public class OptimizedMoveGenerator implements MoveGenerator {
	GameState state;
	int [][] board;
	int [] moveToReturn;
	SearchAlgorithms searchAlgs;
	ArrayList<int []> possibleMoves = new ArrayList<int []> ();
	
	@Override
	public int[] returnMove() {
		findPossibleMoves();
		return null;
	}

	@Override
	public void setGameState(GameState state) {
		moveToReturn = null;
		this.state = state;
		board = this.state.getBoard();
		searchAlgs = new AllMoveSearchAlg (state, state.getPlayer());
	}
	
	public int [] findPossibleMoves() {
		for (int i = 0; i < board.length ; i ++) {
			for (int j = 0; j < board.length ; j++ ) {
				int currentPiece = board[i][j];
				switch(currentPiece) {
					case(0): 
						//do something
						break;
					case(1) :
						if (1 != state.getPlayer()) {
							searchAlgs.checkForMove(i, j);
						}
						break;
					case(2) : 
						if (2 != state.getPlayer()) {
							searchAlgs.checkForMove(i, j);
						}
						break;
				}
				if (!getAllMovesGeneratedAtOnePosition().isEmpty()) {
					ArrayList <int []> toAdd = getAllMovesGeneratedAtOnePosition();
					for(int[] addable : toAdd) {
						if (possibleMoves.contains(addable)) {
							//System.out.println("containsit");
						}
						else {
							//System.out.println(addable[0] + "" + addable[1]);
							possibleMoves.add(addable);
						}
					}
				}
			}
			
		}
		return null;	
	}
	public ArrayList<int[]> getAllMovesGeneratedAtOnePosition() {
		return  ((AllMoveSearchAlg) searchAlgs).getPPF().getMovesToReturn();
	}
	public ArrayList<int[]> getAllMovesFromBoard() {
		return  possibleMoves;
	}
	

}
