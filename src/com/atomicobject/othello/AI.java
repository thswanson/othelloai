package com.atomicobject.othello;


public class AI {
	MoveGenerator generator;
	GameState state;

	public AI(MoveGenerator newGenerator) {
		generator = newGenerator;
	}

	public int[] computeMove(GameState state) {
		generator.setGameState(state);
		System.out.println("AI returning canned move for game state - " + state);	
		return generator.returnMove();
	}
	
	
}
