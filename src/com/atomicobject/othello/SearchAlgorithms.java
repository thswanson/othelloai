package com.atomicobject.othello;

public interface SearchAlgorithms {

	public boolean checkForMove(int row , int col);
	
	public int [] getMoveToReturn();

	
}
