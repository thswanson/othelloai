package com.atomicobject.othello;

public interface MoveGenerator {
	
	
	public int[] returnMove();
	
	public void setGameState(GameState state);

}
