package com.atomicobject.othello;

public class AllMoveSearchAlg implements SearchAlgorithms {

	private GameState state;
	private int[][] board;
	//private int [] moveToReturn;
	private int player;
	private int opposingPlayer;
	private PiecePlaceFinder placeFinder;
	
	public AllMoveSearchAlg(GameState state, int player) {
		this.state = state;
		board = this.state.getBoard();
		placeFinder = new PiecePlaceFinder(board, player);
		//MoveScourer scourer = new MoveScourer(placeFinder);
		setPlayer(player);
	}
	public void setPlayer(int player) {
		if (player == 1 ) {
			this.player = player;
			opposingPlayer = 2;
		}
		else {
			this.player = player;
			opposingPlayer = 1;
		}
	}
	
	@Override
	public boolean checkForMove(int row, int col) {
		checkLeft(row,col);
		checkRight(row,col);
		checkUp(row,col);
		checkDown(row,col);
		checkUpLeft(row,col);
		checkBotRight(row,col);
		checkUpRight(row,col);
		checkBotLeft(row,col);
		if (placeFinder.getMovesToReturn().isEmpty()) return false;
		else { return true; }
	}

	@Override
	public int[] getMoveToReturn() {
		// TODO Auto-generated method stub
		return null;
	}
	private boolean checkBotLeft(int row, int col) {
		for(int i = 1 ; (row + i < board.length && col - i >= 0)  ; i ++ ) {
			if (board[row+i][col-i]== opposingPlayer) continue;
			else if (board[row+i][col-i]== player)  { 
				placeFinder.checkDiagBotLToTopR(row , col , row+i);
				continue; 
			}
			else  { break; }
		}
		return false;
	}

	private boolean checkUpRight(int row, int col) {
		for(int i = 1 ; (row - i >= 0 && col + i < board.length)  ; i ++ ) {
			if (board[row-i][col+i]== opposingPlayer) continue;
			else if (board[row-i][col+i]== player)  { 
				placeFinder.checkDiagBotLToTopR(row , col , row-i);
				continue;
			}
			else  { break; }
		}
		return false;
	}

	private boolean checkBotRight(int row, int col) {
		int max = Integer.max(row, col);
		for(int i = 1 ; i < board.length - max ; i ++ ) {
			if (board[row+i][col+i]== opposingPlayer) continue;
			else if (board[row+i][col+i]== player)  { 
				placeFinder.checkDiagTopLToBotR(row , col , row + i);
				continue;
			}
			else  { break; }
		}
		return false;
	}

	private boolean checkUpLeft(int row, int col) {
		int min = Integer.min(row, col);
		for(int i = 1 ; i <= min ; i ++ ) {
			if (board[row-i][col-i]== opposingPlayer) continue;
			else if (board[row-i][col-i]== player)  { 
				placeFinder.checkDiagTopLToBotR(row , col , row-i);
				continue;
			}
			else  { break; }
		}
		return false;
	}

	private boolean checkDown(int row, int col) {
		for(int i = row+1 ; i < board.length ; i ++ ) {
			if (board[i][col]== opposingPlayer) continue;
			else if (board[i][col]== player)  { 
				placeFinder.checkBottomToTop(row , col , i);
				continue;
			}
			else  { break; }
		}
		return false;
	}

	private boolean checkUp(int row, int col) {
		for ( int i = row-1; i >=0 ; i--) {
			if (board[i][col]== opposingPlayer) continue;
			else if (board[i][col]== player)  { 
				placeFinder.checkBottomToTop(row , col , i);
				continue;
			}
			else  { break; }
		}
		return false;
	}

	private boolean checkLeft(int row, int col) {
		for ( int i = col-1; i >=0 ; i--) {
			if (board[row][i] == opposingPlayer) continue;
			else if (board[row][i]== player)  { 
				placeFinder.checkRightToLeft(row , col , i);
				continue;
			}
			else  { break; }
		}
		return false;
	}
	
	private boolean checkRight(int row, int col) {
		for(int i = col+1 ; i < board.length ; i ++ ) {
			if (board[row][i ]== opposingPlayer) continue;
			else if (board[row][i]== player)  { 
				placeFinder.checkRightToLeft(row , col , i);
				continue;
			}
			else  { break; }
		}
		return false;
	}
	
	public PiecePlaceFinder getPPF() {
		return placeFinder;
	}

}
