Tyler Swanson
AO Take Home Challenge
11/9/2020

Design approach and developement style:
The AI is composed of a move generator interface. A concrete generator has a SearchAlgorithms, which will search for potential move sources, and a PiecePlaceFinder which will take the potential moves and look for a place to put the player's piece. The place to put the player's piece is returned, and sent on to the server to play by the AI and client.

For my development "flow", I used Test driven development(TDD). The goal of this approach was to break the problem down into bitesized pieces by writing tests first and then writing code to pass those tests. This started with me being able to abstract the canned move generator, and allowed me to build the FirstMoveFoundGenerator easily after creating some test cases. The test cases of AITest and AITestPlayer2 show my early testing. TestOptimizedSearchAlg is fairly empty.

My goal in using the interface was to model a flexible software design approach. This interface allows you to create any generator desired for the AI to use. I did my best to continue to develop toward the abstractions with the SearchAlgorithms interface, which may have been overkill. But it allowed the generators to be flexible with its SearchAlgorithms, so you can even flexibly-customize the way generators generate their moves.


Currently Built:
Currently, the FirstMoveFoundGenerator is the only concrete generator that works. It will scan the board for potential move sources and will play the first move that it encounters. I would say that it wins about 50ish percent of the time, especially if its player one.


To launch the project and configure players:
The project is just a normal java project. I have not build it anywhere but eclipse, so I would recommend importing the project into eclipse and launching the main application from there. Otherwise build the project on your own and run it. To switch players, navigate to Main and change the port to 1338 or 1337 depending on player.


Short Comings:
I attempted to build out an optimized move generator. The goal of this was to collect all the possible moves on the board and run an optimizer algorithm to find the place to put a piece anywhere the most pieces change in favor of the player. As I built the code out, I ran out of time and the code began to get away from me. So although disappointing, I was forced to stop development on that part. For not having done something like this before, I feel as though I gave decent effort and made a solid attempt at creating the optimized algorithm.
